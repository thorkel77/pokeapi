var apiUrl = "https://pokeapi.co/api/v2/pokemon/";
    var input = document.querySelector(".pokemon-input");
    var pokemonName = document.querySelector(".pokemon-name");
    var pokemonImage = document.querySelector(".pokemon-image");

    

    function getPokemonData1() {
        axios.get(apiUrl + input.value)
        .then(function (response) {
            pokemonName.innerHTML = response.data.forms[0].name;
            pokemonImage.src = response.data.sprites.front_default;

        })
        .catch(function (error) {
            pokemonName.innerHTML = "le nom est incorrect";
            pokemonImage.src = "";
        });

    }
    function getPokemonData2() {
        axios.get(apiUrl + input.value)
        .then(function (response) {
            var i = 0;
            var type = "";
            
            while(i < response.data.types.length){
            type = type+response.data.types[i].type.name;
            type = type+" ";
            i++;
            }
            pokemonName.innerHTML = type;

        })
        .catch(function (error) {
            pokemonName.innerHTML = "le nom est incorrect";

        });
    }
    function getPokemonData3() {
        axios.get(apiUrl + input.value)
        .then(function (response) {
            var i = 0;
            var talent = "";
            while(i < response.data.abilities.length){
            talent = talent+response.data.abilities[i].ability.name;
            talent = talent+" ";
            i++;
        }
            pokemonName.innerHTML = talent;
        })
        .catch(function (error) {
            pokemonName.innerHTML = "le nom est incorrect";

        });
    }
    function getPokemonData4() {
        axios.get(apiUrl + input.value)
        .then(function (response) {
            var i = 0;
            var j = 0;
            var attaque = "";
            console.log(response.data.moves[i].move.name)
            while(i < response.data.moves.length){
            attaque = attaque+response.data.moves[i].move.name;
            attaque = attaque + " ";
            if (j == 6){
                attaque = attaque+"<br>";
                j = 0;
            }
            i++;
            j++;
        }

           pokemonName.innerHTML = attaque;

        })
        .catch(function (error) {
            pokemonName.innerHTML = "le nom est incorrect";

        });
    }
    function getPokemonData5() {
        axios.get(apiUrl + input.value)
        .then(function (response) {
            stat = "";
            i = 0;
           while(i < response.data.stats.length){
            stat = stat+response.data.stats[i].stat.name;
            stat = stat+" :";
            stat = stat+response.data.stats[i].base_stat;
            stat = stat+"<br>";
            i++;
        
        }
            pokemonName.innerHTML = stat;

        })
        .catch(function (error) {
            pokemonName.innerHTML = "le nom est incorrect";

        });
    }

    var button1 = document.querySelector(".name-button");
    button1.addEventListener("click", getPokemonData1);

    var button2 = document.querySelector(".type-button");
    button2.addEventListener("click", getPokemonData2);
    
    var button3 = document.querySelector(".talent-button");
    button3.addEventListener("click", getPokemonData3);

    var button4 = document.querySelector(".attaque-button");
    button4.addEventListener("click", getPokemonData4);
    
    var button5 = document.querySelector(".stat-button");
    button5.addEventListener("click", getPokemonData5)
    